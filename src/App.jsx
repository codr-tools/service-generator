import { Accordion, AccordionDetails, AccordionSummary, Button, Card, Checkbox, FormControl, FormControlLabel, Grid, InputLabel, MenuItem, Select, Stack, TextField, Typography } from "@mui/material"
import { useState } from "react"

function App() {
  const [serviceName, setServiceName] = useState('app-front-prod')
  const [serviceId, setServiceId] = useState('app-front-prod')
  const [serviceCertResolver, setServiceCertResolver] = useState('production')
  const [servicePort, setServicePort] = useState('80')
  const [serviceRegistry, setServiceRegistry] = useState('registry.gitlab.com')
  const [serviceImage, setServiceImage] = useState('codr.fr/project:latest')
  const [isWww, setWww] = useState(false)
  const [useDb, setDb] = useState(false)
  const dbSubnet = '10.0.3.%';
  const [dbPassword, setDbPassword] = useState('password')
  const [isSecured, setSecured] = useState(true)
  const [serviceDomain, setServiceDomain] = useState('domain.com')
  const [serviceReplicas, setServiceReplicas] = useState('1')
  const [serviceConstraint, setServiceConstraint] = useState('shared')
  const [serviceConstraintOther, setServiceConstraintOther] = useState('custom')
  const [hasSecret, setSecret] = useState(false)
  const [secrets, setSecrets] = useState([
    {
      "key": "API_TOKEN",
      "value": ""
    },
  ])
  const [hasEnv, setEnv] = useState(false)
  const [envs, setEnvs] = useState([
    {
      "key": "API_TOKEN",
      "value": ""
    },
  ])  
  const [secretName, setSecretName] = useState('dotenv')
  const [secretMountPoint, setSecretMountPoint] = useState('/var/www/html/.env')
  const [hasMount, setMount] = useState(false)
  const [mountPoints, setMountPoints] = useState([
    {
      "type": "bind",
      "src": "uploads",
      "dst": "/var/www/html/public",
    },
  ])

  const generateRule = () => {

    const rules = [`\\\`${serviceDomain}\\\``];

    if (isWww) {
      rules.push(`\\\`www.${serviceDomain}\\\``)
    }

    return `Host(${rules.join(', ')})`
  }

  const generateNetworks = () => {
    const networks = []

    networks.push('traefik-public')

    if (useDb) {
      networks.push('database')
    }

    return networks;

  }

  const generateLabels = () => {
    const labels = [];

    labels.push({
      'key': `traefik.enable`,
      'value': `true`,
    })

    labels.push(...generateRoutersLabels())
    labels.push(...generateServicesLabels())

    return labels;
  }

  const generateRoutersLabels = () => {
    const labels = [];
    const prefix = `traefik.http.routers.${serviceId}`

    labels.push({
      'key': `${prefix}.entrypoints`,
      'value': `"web${isSecured ? ', websecure' : ''}"`,
    })

    if (isSecured) {
      labels.push({
        'key': `${prefix}.entrypoints.tls`,
        'value': `"true"`,
      })

      labels.push({
        'key': `${prefix}.tls.certresolver`,
        'value': `"${serviceCertResolver}"`,
      })

      if (isWww) {
        labels.push({
          'key': `${prefix}.tls.domains[0].main`,
          'value': `"${serviceDomain}"`,
        })

        labels.push({
          'key': `${prefix}.tls.domains[0].sans`,
          'value': `"www.${serviceDomain}"`,
        })
      }
    }

    labels.push({
      'key': `${prefix}.rule`,
      'value': `"${generateRule()}"`,
    })

    if (isWww) {
      labels.push({
        'key': `${prefix}.middlewares`,
        'value': `"force-www@file"`,
      })
    }

    return labels;
  }

  const generateServicesLabels = () => {
    const labels = [];
    const prefix = `traefik.http.services.${serviceId}`

    labels.push({
      'key': `${prefix}.loadbalancer.server.port`,
      'value': `${servicePort}`,
    })

    return labels;
  }

  const updateMountPoint = (index, key, value) => {
    const newMountPoints = [...mountPoints];
    newMountPoints[index][key] = value;
    setMountPoints(newMountPoints)
  }

  const removeMountPoint = (index) => {
    const newMountPoints = [...mountPoints];
    newMountPoints.splice(index, 1)
    setMountPoints(newMountPoints)
  }

  const addMountPoint = () => {
    const newMountPoints = [...mountPoints];

    newMountPoints.push({
      "type": "bind",
      "src": "uploads",
      "dst": "/var/www/html/public",
    })

    setMountPoints(newMountPoints)
  }

  const updateSecret = (index, key, value) => {
    const newSecrets = [...secrets];
    newSecrets[index][key] = value;
    setSecrets(newSecrets)
  }

  const removeSecret = (index) => {
    const newSecrets = [...secrets];
    newSecrets.splice(index, 1)
    setSecrets(newSecrets)
  }

  const addSecret = () => {
    const newSecrets = [...secrets];

    newSecrets.push({
      "key": "API_TOKEN",
      "value": "",
    })

    setSecrets(newSecrets)
  }  

  const updateEnv = (index, key, value) => {
    const newEnvs = [...envs];
    newEnvs[index][key] = value;
    setEnvs(newEnvs)
  }

  const removeEnv = (index) => {
    const newEnvs = [...envs];
    newEnvs.splice(index, 1)
    setEnvs(newEnvs)
  }

  const addEnv = () => {
    const newEnvs = [...envs];

    newEnvs.push({
      "key": "API_TOKEN",
      "value": "",
    })

    setEnvs(newEnvs)
  }  

  const [expanded, setExpanded] = useState('panel1');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <Grid container spacing={6}>
      <Grid item xs={4}>

        <Accordion expanded={expanded === 'panelService'} onChange={handleChange('panelService')}>
          <AccordionSummary>
            <Typography>Service</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Stack spacing={2}>
              <TextField label="Service Id" defaultValue={serviceId} onChange={e => setServiceId(e.target.value)} />
              <TextField label="Service Name" defaultValue={serviceName} onChange={e => setServiceName(e.target.value)} />
              <TextField label="Replicas" type="number" defaultValue={serviceReplicas} onChange={e => setServiceReplicas(e.target.value)} />
              <FormControl fullWidth>
                <InputLabel>Constraint</InputLabel>
                <Stack spacing={2}>
                  <Select
                    value={serviceConstraint}
                    label="Constraint"
                    onChange={e => setServiceConstraint(e.target.value)}
                  >
                    <MenuItem value="shared">Shared</MenuItem>
                    <MenuItem value="tools">Tools</MenuItem>
                    <MenuItem value="other">Other</MenuItem>
                  </Select>

                  {
                    serviceConstraint === "other" ? (
                      <>
                        <TextField label="Constraint" defaultValue={serviceConstraintOther} onChange={e => setServiceConstraintOther(e.target.value)} />
                      </>
                    ) : null
                  }
                </Stack>
              </FormControl>
            </Stack>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panelImage'} onChange={handleChange('panelImage')}>
          <AccordionSummary>
            <Typography>Image</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Stack spacing={2}>
              <FormControl fullWidth>
                <InputLabel>Image Registry</InputLabel>
                <Select
                  value={serviceRegistry}
                  label="Image Registry"
                  onChange={e => setServiceRegistry(e.target.value)}
                >
                  <MenuItem value="registry.gitlab.com">Gitlab.com</MenuItem>
                  <MenuItem value="ghcr.io">Github.com</MenuItem>
                  <MenuItem value="public">Docker Hub Public</MenuItem>
                </Select>
              </FormControl>

              <TextField label="Image Name" defaultValue={serviceImage} onChange={e => setServiceImage(e.target.value)} />
              <TextField label="Internal Port" defaultValue={servicePort} onChange={e => setServicePort(e.target.value)} />
            </Stack>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panelSecrets'} onChange={handleChange('panelSecrets')}>
          <AccordionSummary>
            <Typography>Secrets</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Stack spacing={2}>
              <FormControlLabel control={<Checkbox checked={hasSecret} onChange={e => setSecret(e.target.checked)} />} label="Inject Secret" />
              {
                hasSecret ? (
                  <>
                    <TextField label="Secret name" defaultValue={secretName} onChange={e => setSecretName(e.target.value)} />
                    <TextField label="Secret mount point" defaultValue={secretMountPoint} onChange={e => setSecretMountPoint(e.target.value)} />
                  </>
                ) : null
              }

              {
                hasSecret ? (
                  <>
                    {
                      secrets.map((secret, index) => (
                        <>
                          <Card variant="outlined" sx={{ p: 2 }}>
                            <Stack spacing={2}>
                              <TextField label="Key" defaultValue={secret.key} onChange={e => updateSecret(index, 'key', e.target.value)} />
                              <TextField label="Value" defaultValue={secret.value} onChange={e => updateSecret(index, 'value', e.target.value)} />
                              <Button variant="outlined" onClick={e => removeSecret(index)}>Remove</Button>
                            </Stack>
                          </Card>
                        </>
                      ))
                    }
                    <Button variant="outlined" onClick={addSecret}>Add</Button>
                  </>
                ) : null
              }              
            </Stack>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panelEnv'} onChange={handleChange('panelEnv')}>
          <AccordionSummary>
            <Typography>Environment variables</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Stack spacing={2}>
              <FormControlLabel control={<Checkbox checked={hasEnv} onChange={e => setEnv(e.target.checked)} />} label="Set environment variables" />
              {
                hasEnv ? (
                  <>
                    {
                      envs.map((env, index) => (
                        <>
                          <Card variant="outlined" sx={{ p: 2 }}>
                            <Stack spacing={2}>
                              <TextField label="Key" defaultValue={env.key} onChange={e => updateEnv(index, 'key', e.target.value)} />
                              <TextField label="Value" defaultValue={env.value} onChange={e => updateEnv(index, 'value', e.target.value)} />
                              <Button variant="outlined" onClick={e => removeEnv(index)}>Remove</Button>
                            </Stack>
                          </Card>
                        </>
                      ))
                    }
                    <Button variant="outlined" onClick={addEnv}>Add</Button>
                  </>
                ) : null
              }
            </Stack>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panelMounts'} onChange={handleChange('panelMounts')}>
          <AccordionSummary>
            <Typography>Mounts</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Stack spacing={2}>
              <FormControlLabel control={<Checkbox checked={hasMount} onChange={e => setMount(e.target.checked)} />} label="Add Mount points" />
              {
                hasMount ? (
                  <>
                    {
                      mountPoints.map((mount, index) => (
                        <>
                          <Card variant="outlined" sx={{ p: 2 }}>
                            <Stack spacing={2}>
                              <FormControl fullWidth>
                                <InputLabel>Mount type</InputLabel>
                                <Select
                                  value={mount.type}
                                  label="Mount type"
                                  onChange={e => updateMountPoint(index, 'type', e.target.value)}
                                >
                                  <MenuItem value="bind">bind</MenuItem>
                                  <MenuItem value="volume">volume</MenuItem>
                                  <MenuItem value="tmpfs">tmpfs</MenuItem>
                                </Select>
                              </FormControl>
                              <TextField label="Mount src" defaultValue={mount.src} onChange={e => updateMountPoint(index, 'src', e.target.value)} />
                              <TextField label="Mount dst" defaultValue={mount.dst} onChange={e => updateMountPoint(index, 'dst', e.target.value)} />
                              <Button variant="outlined" onClick={e => removeMountPoint(index)}>Remove</Button>
                            </Stack>
                          </Card>
                        </>
                      ))
                    }
                    <Button variant="outlined" onClick={addMountPoint}>Add</Button>
                  </>
                ) : null
              }
            </Stack>

          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panelDomain'} onChange={handleChange('panelDomain')}>
          <AccordionSummary>
            <Typography>Domain</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Stack spacing={2}>
              <TextField label="Domain" defaultValue={serviceDomain} onChange={e => setServiceDomain(e.target.value)} />
              <FormControlLabel control={<Checkbox checked={isWww} onChange={e => setWww(e.target.checked)} />} label="Add www" />
              <FormControlLabel control={<Checkbox checked={isSecured} onChange={e => setSecured(e.target.checked)} />} label="Add SSL" />
              {
                isSecured ? (
                  <FormControl fullWidth>
                    <InputLabel>Cert resolver</InputLabel>
                    <Select
                      value={serviceCertResolver}
                      label="Cert resolver"
                      onChange={e => setServiceCertResolver(e.target.value)}
                    >
                      <MenuItem value="staging">Staging</MenuItem>
                      <MenuItem value="production">Production</MenuItem>
                    </Select>
                  </FormControl>
                ) : null
              }
            </Stack>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panelDatabase'} onChange={handleChange('panelDatabase')}>
          <AccordionSummary>
            <Typography>Database</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Stack spacing={2}>
              <FormControlLabel control={<Checkbox checked={useDb} onChange={e => setDb(e.target.checked)} />} label="Add Database" />

              {
                useDb ? (
                  <TextField label="Password" defaultValue={dbPassword} onChange={e => setDbPassword(e.target.value)} />
                ) : null
              }
            </Stack>
          </AccordionDetails>
        </Accordion>
      </Grid>

      <Grid item xs={8}>
        <Card variant="outlined" sx={{ p: 2 }}>
          <code>
            {
              useDb ? (
                <>
                  # Create MySQL DB and USER<br />
                  <br />
                  CREATE DATABASE `{serviceName}`;<br />
                  CREATE USER `{serviceName}`@{dbSubnet} IDENTIFIED BY "{dbPassword}";<br />
                  GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, INDEX, DROP, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON `{serviceName}`.* TO `{serviceName}`@{dbSubnet};<br />
                  FLUSH PRIVILEGES;<br />
                  <br />
                </>
              ) : null
            }

            {
              hasSecret ? (
                <>
                  # Prepare secrets<br />
                  # Name: {serviceId}-{secretName}
                  <br />
                  {
                    secrets.map(secret => <>{secret.key}={secret.value}<br /></>)
                  }
                  <br />
                </>
              ) : null
            }

            {
              hasMount ? (
                <>
                  # Prepare NFS directory<br />
                  <br />
                  {
                    mountPoints.map(mount => (
                      <>
                        sudo mkdir -p /mnt/nfs_share/{serviceId}/{mount.src}<br />
                      </>
                    ))
                  }
                  <br />
                  sudo chown -R ubuntu:ubuntu /mnt/nfs_share/{serviceId}<br />
                  <br />
                </>
              ) : null
            }

            {
              serviceRegistry !== 'public' ? (
                <>
                  # Log into registry<br />
                  <br />
                  
                  docker login {serviceRegistry}<br />
                  <br />
                </>
              ) : null
            }

            # Remove existing service<br />
            <br />
            docker service rm {serviceName} || true<br />
            <br />
            # Create new service<br />
            <br />
            docker service create \<br />
            --with-registry-auth \<br />
            {
              generateNetworks().map(network => <>--network {network} \<br /></>)
            }
            --constraint node.labels.group=={serviceConstraint !== 'other' ? serviceConstraint : serviceConstraintOther} \<br />
            --update-order start-first \<br />
            --replicas {serviceReplicas} \<br />
            --name {serviceName} \<br />
            {
              generateLabels().map(label => <>--label {label.key}={label.value} \<br /></>)
            }
            {
              hasEnv ? envs.map(env => <>--env {env.key}={env.value} \<br /></>) : null
            }
            {
              hasMount ? mountPoints.map(mount => <>--mount type={mount.type},src=/data/nfs/{serviceId}/{mount.src},dst={mount.dst} \<br /></>) : null
            }
            {
              hasSecret ? <>--secret source={serviceId}-{secretName},target="{secretMountPoint}" \<br /></> : null
            }
            {
              serviceRegistry !== 'public' ? <>{serviceRegistry}/{serviceImage}</> : <>{serviceImage}</>
            }
          </code>
        </Card>
      </Grid>
    </Grid>
  )
}

export default App
